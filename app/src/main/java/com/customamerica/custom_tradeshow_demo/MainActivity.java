package com.customamerica.custom_tradeshow_demo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mikepenz.aboutlibraries.LibsBuilder;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import it.custom.printer.api.android.CustomAndroidAPI;

/**
 * Created by Bill Bagdon on 8/20/2014.
 */
public class MainActivity extends AppCompatActivity implements PrinterFragment.OnFragmentInteractionListener {

    private static final String APP_PREFERENCES = "AppPrefs";
    private static final String CE_LIBLOB = "LibLog";
    private static final String TS_NAME = "TradeshowName";
    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private Drawer mDrawer;
    private SharedPreferences sharedpreferences;
    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withHeader(R.layout.header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.welcome).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_accessibility)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(0),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.wifi_protocol).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_wifi)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.bluetooth_protocol).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_bluetooth)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.usb_protocol).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_usb)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.serial_protocol).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_swap_horiz)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(4),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(getString(R.string.about)).withIcon(
                                new IconicsDrawable(this)
                                        .icon(GoogleMaterial.Icon.gmd_info_outline)
                                        .sizeDp(24))
                                .withIconTintingEnabled(true).withIdentifier(5)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        currentPosition = position;
                        if (drawerItem.getIdentifier() <= 4) {
                            mDrawer.closeDrawer();
                            fragmentSwitch(drawerItem.getIdentifier());
                        } else {
                            mDrawer.closeDrawer();
                            new LibsBuilder()
                                    //Pass the fields of your application to the lib so it can find all external lib information
                                    .withFields(R.string.class.getFields())
                                    .withActivityTheme(R.style.AboutTheme)
                                    .withAboutIconShown(true)
                                    .withAboutVersionShown(true)
                                    .withAboutDescription(getString(R.string.api_version_text) + CustomAndroidAPI.getAPIVersion())
                                            //start the activity
                                    .start(view.getContext());
                        }
                        return true;
                    }
                })
                .withSelectedItem(currentPosition)
                .build();

        sharedpreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(CE_LIBLOB)) {
            CustomAndroidAPI.EnableLogAPI(sharedpreferences.getBoolean(CE_LIBLOB, false));
        }
        if (sharedpreferences.contains(TS_NAME)) {
            sharedpreferences.getString(TS_NAME, "Tradeshow");
        }

        fragmentSwitch(currentPosition);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mDrawer.setSelection(0);
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        currentPosition = savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM);
        fragmentSwitch(currentPosition);
        mDrawer.setSelection(currentPosition);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                currentPosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            fragmentSwitch(currentPosition);
            return true;
        } else if (id == R.id.action_rename) {
            renameDialog();
        } else if (id == R.id.action_log) {
            logDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void renameDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.rename_dialog);
        dialog.setTitle(getString(R.string.tradeshow_rename_title));

        Button button = (Button) dialog.findViewById(R.id.rename_dialog_accept);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                EditText edit = (EditText) dialog.findViewById(R.id.rename_dialog_edit_text);
                String text = edit.getText().toString();

                dialog.dismiss();

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(TS_NAME, text);
                editor.apply();
            }
        });

        dialog.show();
    }

    private void fragmentSwitch(int position) {
        String instance;
        switch (position) {
            case 0:
                instance = "";
                break;
            case 1:
                instance = getString(R.string.wifi_protocol);
                break;
            case 2:
                instance = getString(R.string.bluetooth_protocol);
                break;
            case 3:
                instance = getString(R.string.usb_protocol);
                break;
            case 4:
                instance = getString(R.string.serial_protocol);
                break;
            default:
                instance = "";
                break;
        }
        if (!instance.equals("")) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, PrinterFragment.newInstance(instance))
                    .commit();
        } else {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, WelcomeFragment.newInstance())
                    .commit();
        }
        currentPosition = position;
    }

    private void logDialog() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final boolean logEnabled;
        String s1;
        String s2;

        //check to see if the log is already enabled in the preferences
        logEnabled = sharedpreferences.contains(CE_LIBLOB) && sharedpreferences.getBoolean(CE_LIBLOB, false);

        //help to create the correct dialog based on the already known preferences
        if (logEnabled) {
            s1 = "enabled";
            s2 = "disable";
        } else {
            s1 = "disabled";
            s2 = "enable";
        }
        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(this.getString(R.string.log_enable_message, s1, s2))
                .setTitle(R.string.log_enable_title);

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(CE_LIBLOB, !logEnabled);
                editor.apply();
                CustomAndroidAPI.EnableLogAPI(!logEnabled);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}