package com.customamerica.custom_tradeshow_demo;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class WelcomeFragment extends Fragment {

    private static final String APP_PREFERENCES = "AppPrefs";
    private static final String TS_NAME = "TradeshowName";

    public WelcomeFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static WelcomeFragment newInstance() {
        WelcomeFragment fragment = new WelcomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_welcome, container, false);
        TextView mWelcomeText = (TextView) mView.findViewById(R.id.welcome_text);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String tradeshowName = "";

        if (sharedpreferences.contains(TS_NAME)) {
            tradeshowName = sharedpreferences.getString(TS_NAME, "the Tradeshow");
        }

        mWelcomeText.setText(getString(R.string.tradeshow_welcome, tradeshowName));

        return mView;
    }

}
