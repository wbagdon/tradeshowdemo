package com.customamerica.custom_tradeshow_demo;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import it.custom.printer.api.android.CustomAndroidAPI;
import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;
import it.custom.printer.api.android.PrinterFont;

public class PrinterFragment extends ListFragment {

    private static final String ARG_PARAM1 = "param1";

    @Nullable
    public static CustomPrinter prnDevice;
    public static PrinterFont fontNormal;
    public static PrinterFont fontHeader;
    public static PrinterFont fontRight;
    public static PrinterFont fontBigHeader;

    private String mParamProtocol;
    @Nullable
    private OnFragmentInteractionListener mListener;
    private PrinterAdapter mPrinterAdapter;
    private ListView mPrinterList;
    private View mHeader;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PrinterFragment() {
    }

    @NonNull
    public static PrinterFragment newInstance(String paramProtocol) {
        PrinterFragment fragment = new PrinterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, paramProtocol);
        fragment.setArguments(args);
        return fragment;
    }

    private static void setupFonts() {
        fontNormal = new PrinterFont();
        fontHeader = new PrinterFont();
        fontRight = new PrinterFont();
        fontBigHeader = new PrinterFont();
        try {
            fontHeader.setEmphasized(true);
            fontHeader.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);

            fontRight.setJustification(PrinterFont.FONT_JUSTIFICATION_RIGHT);

            fontBigHeader.setEmphasized(true);
            fontBigHeader.setJustification(PrinterFont.FONT_JUSTIFICATION_CENTER);
            fontBigHeader.setCharHeight(2);
            fontBigHeader.setCharWidth(1);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParamProtocol = getArguments().getString(ARG_PARAM1);
        }

        mPrinterAdapter = new PrinterAdapter(getActivity(), getActivity().getLayoutInflater());
        mPrinterList = (ListView) getActivity().findViewById(R.id.printer_list);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        mHeader = inflater.inflate(R.layout.printerlist_header, mPrinterList, false);

        if (mParamProtocol.equals(getString(R.string.wifi_protocol))) {
            mPrinterAdapter.updateEthDeviceList();
        } else if (mParamProtocol.equals(getString(R.string.bluetooth_protocol))) {
            mPrinterAdapter.updateBTDeviceList();
        } else if (mParamProtocol.equals(getString(R.string.usb_protocol))) {
            mPrinterAdapter.updateUSBDeviceList();
        } else if (mParamProtocol.equals(getString(R.string.serial_protocol))) {
            mPrinterAdapter.updateSerialDeviceList();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mHeader != null) {
            getListView().addHeaderView(mHeader, null, false);
            TextView mPrinterListText = (TextView) mHeader.findViewById(R.id.printer_list_connection_text);
            if (mParamProtocol.equals(getString(R.string.wifi_protocol))) {
                mPrinterListText.setText("{gmd-wifi} " + mParamProtocol);
            } else if (mParamProtocol.equals(getString(R.string.bluetooth_protocol))) {
                mPrinterListText.setText("{gmd-bluetooth} " + mParamProtocol);
            } else if (mParamProtocol.equals(getString(R.string.usb_protocol))) {
                mPrinterListText.setText("{gmd-usb} " + mParamProtocol);
            } else if (mParamProtocol.equals(getString(R.string.serial_protocol))) {
                mPrinterListText.setText("{gmd-swap-horiz} " + mParamProtocol);
            }
        }
        setListAdapter(mPrinterAdapter);
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(@NonNull ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (null != mListener) {
            getActivity().setProgressBarIndeterminateVisibility(true);
            new PrintTask().execute(((PrinterAdapter.PrinterWithName) l.getItemAtPosition(position)).getPrinter(), "", false);
            getActivity().setProgressBarIndeterminateVisibility(false);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }

    private class PrintTask extends AsyncTask<Object, String, Boolean> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getString(R.string.connecting_to_printer));
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.show();
            }
        }

        @NonNull
        @Override
        protected Boolean doInBackground(Object... params) {

            if (openDevice(params[0])) {
                openActivity();
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (!success) {
                SnackbarManager.show(
                        Snackbar.with(getActivity()) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(getActivity().getString(R.string.printer_not_connected)) // text to display
                );
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        //Open the device if it isn't already opened
        private boolean openDevice(Object noClassPrinter) {
            closeDevice();
            try {
                if (mParamProtocol.equals(getString(R.string.bluetooth_protocol))) {
                    BluetoothDevice tempDev = (BluetoothDevice) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverBT(tempDev);
                } else if (mParamProtocol.equals(getString(R.string.wifi_protocol))) {
                    String tempDev = (String) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverETH(tempDev);
                } else if (mParamProtocol.equals(getString(R.string.usb_protocol))) {
                    UsbDevice tempDev = (UsbDevice) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverUSB(tempDev, getActivity());
                } else if (mParamProtocol.equals(getString(R.string.serial_protocol))) {
                    String tempDev = (String) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverCOM(tempDev);
                }
                return true;
            } catch (CustomException e) {
                //Show Error
                Log.d("Error...", e.getMessage());
                return false;
            } catch (Exception e) {
                Log.d("Error...", "Open Print Error...");
                //open error
                return false;
            }
        }

        private void openActivity() {
            if (fontNormal == null) {
                setupFonts();
            }

            Intent intentToOpen;
            assert prnDevice != null;
            String printerName = prnDevice.getPrinterName().toUpperCase();

            if (prnDevice.isScannerAvailable()) {
                intentToOpen = new Intent(getActivity(), ScannerActivity.class);
            } else if (printerName.contains("VKP80III")) {
                intentToOpen = new Intent(getActivity(), TITOActivity.class);
            } else if (printerName.contains("TK302") || printerName.contains("TK180") || printerName.contains("B202")) {
                intentToOpen = new Intent(getActivity(), AeroActivity.class);
            } else if (printerName.contains("K3") || printerName.contains("KUBE") || printerName.contains("Q") || printerName.contains("D-ONE") || printerName.contains("FUSION")) {
                intentToOpen = new Intent(getActivity(), POSActivity.class);
            } else {
                intentToOpen = new Intent(getActivity(), PrintActivity.class);
            }
            startActivity(intentToOpen);
        }

        private void closeDevice() {
            if (prnDevice != null) {
                try {
                    prnDevice.close();
                } catch (CustomException e) {
                    e.printStackTrace();
                }
            }
            prnDevice = null;
        }

    }

}

