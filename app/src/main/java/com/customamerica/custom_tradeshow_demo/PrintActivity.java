package com.customamerica.custom_tradeshow_demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;

public class PrintActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String APP_PREFERENCES = "AppPrefs";
    private static final String TS_NAME = "TradeshowName";
    private final byte[] printSetup = {0x1d, (byte) 0xff, 0x68, 0x30, (byte) 0xb4};
    private CustomPrinter prnDevice;
    private String printerName;
    private String tradeshowName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        prnDevice = PrinterFragment.prnDevice;
        assert prnDevice != null;
        printerName = prnDevice.getPrinterName();

        Toolbar mToolbar = (Toolbar) findViewById(R.id.activity_demo_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedpreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(TS_NAME)) {
            tradeshowName = sharedpreferences.getString(TS_NAME, "the Tradeshow");
        }

        TextView activityText = (TextView) findViewById(R.id.print_activity_text);
        activityText.setText(printerName);

        Button printButton = (Button) findViewById(R.id.print_button);
        printButton.setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            prnDevice.close();
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.print, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_print_setup) {
            printSetup();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        print();
    }

    private void print() {
        try {

            String fwVersion;

            if (printerName.contains("Q1")) {
                printerName = "Custom Q3";
            } else if (printerName.trim().toUpperCase().equals("CUSTOM VKP80")) {
                printerName = "Custom VKP80II";
            }
            int printWidth = prnDevice.getPageWidth();

            fwVersion = prnDevice.getFirmwareVersion();

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.custom_logo);
            prnDevice.printImage(bm, CustomPrinter.IMAGE_ALIGN_TO_CENTER, CustomPrinter.IMAGE_SCALE_TO_WIDTH, 300);
            prnDevice.feed(1);

            prnDevice.printTextLF(getString(R.string.address1), PrinterFragment.fontHeader);
            prnDevice.printTextLF(getString(R.string.address2), PrinterFragment.fontHeader);
            prnDevice.printTextLF(getString(R.string.phone), PrinterFragment.fontHeader);
            prnDevice.feed(2);

            if (printWidth < 576) {
                prnDevice.printTextLF(getString(R.string.tradeshow_welcome, tradeshowName), PrinterFragment.fontHeader);
            } else {
                prnDevice.printTextLF(getString(R.string.tradeshow_welcome, tradeshowName), PrinterFragment.fontBigHeader);
            }
            prnDevice.feed(2);

            prnDevice.printTextLF(getString(R.string.ticket_printer_name_text), PrinterFragment.fontNormal);
            prnDevice.printTextLF(printerName, PrinterFragment.fontRight);

            prnDevice.printTextLF(getString(R.string.ticket_firmware_text), PrinterFragment.fontNormal);
            prnDevice.printTextLF(fwVersion, PrinterFragment.fontRight);

            prnDevice.feed(2);

            prnDevice.printTextLF(getString(R.string.ticket_scan_text), PrinterFragment.fontHeader);
            prnDevice.feed(1);

            String url = getString(R.string.base_brochure_url);
            if (printerName.toUpperCase().contains("K3")) {
                url += getString(R.string.k3_url);
            } else if (printerName.toUpperCase().contains("Q3")) {
                url += getString(R.string.q3_url);
            } else if (printerName.toUpperCase().contains("MY3")) {
                url += getString(R.string.my3_url);
            } else if (printerName.toUpperCase().contains("MYP")) {
                url += getString(R.string.my_url);
            } else if (printerName.toUpperCase().contains("VKP80III")) {
                url += getString(R.string.vkp80iii_url);
            } else if (printerName.toUpperCase().contains("VKP80II")) {
                url += getString(R.string.vkp80ii_url);
            } else if (printerName.toUpperCase().contains("TG2460H")) {
                url += getString(R.string.tg2460h_url);
            } else {
                url = getString(R.string.custom_url);
            }
            prnDevice.printBarcode2D(url, CustomPrinter.BARCODE_TYPE_QRCODE, CustomPrinter.BARCODE_ALIGN_TO_CENTER, 150);
            prnDevice.feed(4);

            if (printerName.contains("TG2460H")) {
                prnDevice.feed(6);
            }

            if (printerName.contains("VKP80II")) {
                prnDevice.present(60);
            } else if (printerName.contains("TG2480")) {
                prnDevice.cut(CustomPrinter.CUT_TOTAL);
            } else {
                prnDevice.cut(CustomPrinter.CUT_PARTIAL);
            }

        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    private void printSetup() {
        try {
            prnDevice.writeData(printSetup);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }
}
