package com.customamerica.custom_tradeshow_demo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;
import it.custom.printer.api.android.ScannerImage;

public class ScannerActivity extends AppCompatActivity implements OnClickListener {

    @Nullable
    private CustomPrinter prnDevice;
    private ImageView imageView;
    private Context mContext;
    private int currentImageID;
    private int nextImageID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        mContext = this;

        Toolbar mToolbar = (Toolbar) findViewById(R.id.activity_demo_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //set header to be the printer name
        TextView demoTitle = (TextView) findViewById(R.id.activity_text);
        prnDevice = PrinterFragment.prnDevice;
        demoTitle.setText(prnDevice != null ? prnDevice.getPrinterName() : null);

        //set up image view to hold scanned image
        imageView = (ImageView) findViewById(R.id.scan_image);

        //setup scan button
        Button scan = (Button) findViewById(R.id.scan_button);
        scan.setOnClickListener(this);


        try {
            currentImageID = prnDevice.scannerGetImageScannedID();
            nextImageID = currentImageID;
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.scanner, menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            assert prnDevice != null;
            prnDevice.close();
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        //set progress bar before starting a loop to wait for a scan
        setSupportProgressBarIndeterminateVisibility(true);

        //start async task to wait for scanned document
        new ScanTask(mContext).execute(null, null, false);
        //remove progress bar
        setSupportProgressBarIndeterminateVisibility(false);
    }

    private class ScanTask extends AsyncTask<Object, String, Boolean> {

        private final Context mContext;
        ScannerImage scannerImage;
        private ProgressDialog progressDialog;

        //constructor to give context for progress dialog
        public ScanTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            //create progress dialog that will not end until a document is scanned
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(getString(R.string.waitingToScan));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            //reset ImageView to no image so it can be filled with a new document
            imageView.setImageBitmap(null);

            try {
                //enable the feeder on the scanner and make the scanner hold the document
                prnDevice.scannerSetSearchType(CustomPrinter.SCANNER_SEARCH_NONE, true);
                prnDevice.scannerFeeder(CustomPrinter.SCANNER_FEEDER_ENABLE);
                prnDevice.scannerSetEndScanMovement(CustomPrinter.SCANNER_AUTOM_HOLD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        protected Boolean doInBackground(Object... params) {
            try {
                //wait for an image to be created by the scanner
                while (currentImageID == nextImageID) {
                    nextImageID = prnDevice != null ? prnDevice.scannerGetImageScannedID() : 0;
                    Thread.sleep(100);
                }
                //eject the document, disable the scanner feeder
                prnDevice.scannerEject(CustomPrinter.SCANNER_EJ_EJECT);
                prnDevice.scannerFeeder(CustomPrinter.SCANNER_FEEDER_DISABLE);
                currentImageID = nextImageID;
                //get and set the scanned document to the ImageView
                scannerImage = prnDevice.scannerGetImage(CustomPrinter.SCANNER_IMAGE_GRAY_256, true);
            } catch (CustomException e) {
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(e.getMessage()) // text to display
                );
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (scannerImage != null) {
                imageView.setImageBitmap(scannerImage.Image);
            }
            //dismiss the progress dialog
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }
}
