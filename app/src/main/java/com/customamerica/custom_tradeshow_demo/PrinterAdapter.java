package com.customamerica.custom_tradeshow_demo;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.io.File;

import custom.android_serialport_api.SerialPort;
import custom.android_serialport_api.SerialPortFinder;
import it.custom.printer.api.android.CustomAndroidAPI;
import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;

public class PrinterAdapter extends BaseAdapter {

    private static String adapterType;
    @Nullable
    private static PrinterWithName[] printerList;

    private final Context mContext;
    private final LayoutInflater mInflater;

    public PrinterAdapter(Context context, LayoutInflater inflater) {
        mContext = context;
        mInflater = inflater;
        adapterType = "";
        printerList = null;
    }

    @Override
    public int getCount() {
        if (printerList != null) {
            return printerList.length;
        } else {
            return 0;
        }
    }

    @Nullable
    @Override
    public PrinterWithName getItem(int position) {
        assert printerList != null;
        return printerList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        ViewHolder holder;
        // check if the view already exists
        // if so, no need to inflate and findViewById again!
        if (convertView == null) {

            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.row_printer, parent, false);

            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.img_thumbnail);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.text_title);

            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        } else {

            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        PrinterWithName prn = getItem(position);

        assert prn != null;
        if (!prn.isNameSet()) {
            new GetNameTask().execute(prn);
            Log.d("Naming", holder.titleTextView.getText().toString());
        }

        String printerName = prn.getPrinterName().toUpperCase();
        holder.titleTextView.setText(printerName);
        if (printerName.contains("MY3")) {
            holder.thumbnailImageView.setImageResource(R.drawable.my3);
        } else if (printerName.contains("KPM30")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kpm302);
        } else if (printerName.contains("MYP")) {
            holder.thumbnailImageView.setImageResource(R.drawable.my);
        } else if (printerName.contains("K3")) {
            holder.thumbnailImageView.setImageResource(R.drawable.k3);
        } else if (printerName.contains("Q3")) {
            holder.thumbnailImageView.setImageResource(R.drawable.q3);
        } else if (printerName.contains("D-ONE")) {
            holder.thumbnailImageView.setImageResource(R.drawable.done);
        } else if (printerName.contains("VKP80III")) {
            holder.thumbnailImageView.setImageResource(R.drawable.vkp80iii);
        } else if (printerName.contains("VKP80")) {
            holder.thumbnailImageView.setImageResource(R.drawable.vkp80ii);
        } else if (printerName.contains("TG2460")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tg2460h);
        } else if (printerName.contains("TG2480")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tg2480h);
        } else if (printerName.contains("TL60")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tl60);
        } else if (printerName.contains("TL80")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tl80);
        } else if (printerName.contains("KUBE SCANNER")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kubescan);
        } else if (printerName.contains("KUBE")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kubeii);
        } else if (printerName.contains("VK80")) {
            holder.thumbnailImageView.setImageResource(R.drawable.vk80);
        } else if (printerName.contains("KM216")) {
            holder.thumbnailImageView.setImageResource(R.drawable.km216);
        } else if (printerName.contains("KPM150")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kpm150h);
        } else if (printerName.contains("KPM180")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kpm180h);
        } else if (printerName.contains("TPTCM60")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tptcm60);
        } else if (printerName.contains("TPTCM112")) {
            holder.thumbnailImageView.setImageResource(R.drawable.tptcm112);
        } else if (printerName.contains("VKP112")) {
            holder.thumbnailImageView.setImageResource(R.drawable.vkp112);
        } else if (printerName.contains("KPM216")) {
            holder.thumbnailImageView.setImageResource(R.drawable.kpm216h);
        } else if (printerName.contains("FUSION")) {
            holder.thumbnailImageView.setImageResource(R.drawable.fusion);
        } else {
            holder.thumbnailImageView.setImageResource(R.drawable.ic_printer);
        }
        return convertView;
    }

    public void updateEthDeviceList() {
        Log.d("Protocol", "Ethernet");
        printerList = null;
        String[] ethDeviceList;
        try {
            //Get the list of devices (search for 1.5 seconds)
            ethDeviceList = CustomAndroidAPI.EnumEthernetDevices(1500, mContext);

            if ((ethDeviceList == null) || (ethDeviceList.length == 0)) {
                //Show Error
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(mContext.getString(R.string.no_device_discovered, mContext.getString(R.string.wifi_protocol))) // text to display
                );
                return;
            } else {
                printerList = new PrinterWithName[ethDeviceList.length];
                for (int i = 0; i < ethDeviceList.length; i++) {
                    printerList[i] = new PrinterWithName();
                    printerList[i].setPrinter(ethDeviceList[i]);
                }
            }
        } catch (Exception e) {
            //Show Error
            Log.d("Protocol", "Eth-" + e.toString());
            return;
        }
        adapterType = mContext.getString(R.string.wifi_protocol);
        notifyDataSetChanged();
    }

    public void updateBTDeviceList() {
        Log.d("Protocol", "BT");
        BluetoothDevice[] btDeviceList;
        printerList = null;
        try {
            //Get the list of devices
            btDeviceList = CustomAndroidAPI.EnumBluetoothDevices();

            if ((btDeviceList == null) || (btDeviceList.length == 0)) {
                //Show Error
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(mContext.getString(R.string.no_device_discovered, mContext.getString(R.string.bluetooth_protocol))) // text to display
                );
                return;
            } else {
                printerList = new PrinterWithName[btDeviceList.length];
                for (int i = 0; i < btDeviceList.length; i++) {
                    printerList[i] = new PrinterWithName();
                    printerList[i].setPrinter(btDeviceList[i]);
                }
            }
        } catch (CustomException e) {
            //Show Error
            if (e.getMessage().toUpperCase().contains("ERROR 8")) {
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(mContext.getString(R.string.enable_bt_reminder)) // text to display
                );
            }
            Log.d("Protocol", "BT-" + e.toString());
            return;
        } catch (Exception e) {
            //Show Error
            Log.d("Protocol", "BT-" + e.toString());
            return;
        }
        adapterType = mContext.getString(R.string.bluetooth_protocol);
        notifyDataSetChanged();
    }

    public void updateUSBDeviceList() {
        Log.d("Protocol", "USB");
        UsbDevice[] usbDeviceList;
        printerList = null;
        try {
            usbDeviceList = CustomAndroidAPI.EnumUsbDevices(mContext);
            if ((usbDeviceList == null) || (usbDeviceList.length == 0)) {
                //Show Error
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(mContext.getString(R.string.no_device_discovered, mContext.getString(R.string.usb_protocol))) // text to display
                );
                return;
            } else {
                printerList = new PrinterWithName[usbDeviceList.length];
                for (int i = 0; i < usbDeviceList.length; i++) {
                    printerList[i] = new PrinterWithName();
                    printerList[i].setPrinter(usbDeviceList[i]);
                }
            }
        } catch (CustomException e) {
            Log.d("Protocol", "USB-" + e.toString());
        }
        adapterType = mContext.getString(R.string.usb_protocol);
        notifyDataSetChanged();
    }

    public void updateSerialDeviceList() {
        Log.d("Protocol", "COM");
        printerList = null;

        //Get the list of devices (search for 1.5 seconds)
        String[] availableComPorts;
        availableComPorts = new SerialPortFinder().getAllDevicesPath();

        //setup for Fusion device
        String fusionComDevice = mContext.getString(R.string.fusion_com_port);
        boolean fusionExists = false;

        if (availableComPorts != null) {
            //search for Fusion COM port
            for (String availableComPort : availableComPorts) {
                if (availableComPort.equals(fusionComDevice)) {
                    fusionExists = true;
                }
            }
            if (fusionExists) {
                try {
                    printerList = new PrinterWithName[1];
                    SerialPort sp;
                    //Open and set the configuration object
                    try {
                        //Set com port parameters
                        sp = new SerialPort(new File(fusionComDevice), 115200, SerialPort.NON_BLOCKING);
                        sp.setParameters(SerialPort.DATABITS_8, SerialPort.PARITY_NONE, SerialPort.STOPBITS_1, SerialPort.FLOWCONTROL_RTSCTS);
                    } catch (Exception ex) {
                        sp = null;
                    }
                    //Close the configuration object
                    if (sp != null) {
                        Log.d("COMPORT", "Closed COM Port");
                        sp.Close();
                    }
                    printerList[0] = new PrinterWithName();
                    printerList[0].setPrinter(fusionComDevice);
                } catch (Exception e) {
                    //Show Error
                    Log.d("Protocol", "COM-" + e.toString());
                    return;
                }
            } else {
                //Show Error
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(mContext.getString(R.string.no_device_discovered, mContext.getString(R.string.serial_protocol))) // text to display
                );
                return;
            }
        } else {
            //Show Error
            SnackbarManager.show(
                    Snackbar.with(mContext) // context
                            .colorResource(R.color.material_drawer_accent)
                            .text(mContext.getString(R.string.no_device_discovered, mContext.getString(R.string.serial_protocol))) // text to display
            );
            return;
        }
        adapterType = mContext.getString(R.string.serial_protocol);
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        public ImageView thumbnailImageView;
        public TextView titleTextView;
    }

    public class PrinterWithName {
        public Object prnNoClass;
        public String printerName = "Custom Printer";
        public boolean nameSet = false;

        public Object getPrinter() {
            return prnNoClass;
        }

        public void setPrinter(Object prn) {
            prnNoClass = prn;
        }

        public String getPrinterName() {
            return printerName;
        }

        public void setPrinterName(String name) {
            nameSet = true;
            printerName = name;
        }

        public boolean isNameSet() {
            Log.d("Naming", "Name was set to " + printerName);
            return nameSet;
        }
    }

    private class GetNameTask extends AsyncTask<PrinterWithName, String, Boolean> {

        PrinterWithName printer;
        @Nullable
        CustomPrinter prnDevice;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            Log.d("Naming - pre", "Pre-execute");
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(mContext.getString(R.string.connecting_to_printer));
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.show();
            }
        }

        @NonNull
        @SuppressWarnings("StringEquality")
        @Override
        protected Boolean doInBackground(PrinterWithName... params) {
            printer = params[0];
            if (adapterType != mContext.getString(R.string.bluetooth_protocol)) {
                String name = openDevice(params[0].getPrinter());
                printer.setPrinterName(name);
            } else {
                String name = ((BluetoothDevice) (params[0].prnNoClass)).getName();
                printer.setPrinterName(name);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            Log.d("Naming - post", "Something completed");
            closeDevice();
            notifyDataSetChanged();
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }


        private String openDevice(Object noClassPrinter) {
            closeDevice();
            try {
                if (adapterType.equals(mContext.getString(R.string.bluetooth_protocol))) {
                    BluetoothDevice tempDev = (BluetoothDevice) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverBT(tempDev);
                } else if (adapterType.equals(mContext.getString(R.string.wifi_protocol))) {
                    String tempDev = (String) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverETH(tempDev);
                } else if (adapterType.equals(mContext.getString(R.string.usb_protocol))) {
                    UsbDevice tempDev = (UsbDevice) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverUSB(tempDev, mContext);
                } else if (adapterType.equals(mContext.getString(R.string.serial_protocol))) {
                    String tempDev = (String) noClassPrinter;
                    prnDevice = new CustomAndroidAPI().getPrinterDriverCOM(tempDev);
                }
                assert prnDevice != null;
                return prnDevice.getPrinterName();
            } catch (CustomException e) {
                //Show Error
                Log.d("Error...", e.getMessage());
                if (adapterType.equals(mContext.getString(R.string.serial_protocol))) {
                    return (String) noClassPrinter;
                } else {
                    return "Not Available";
                }
            } catch (Exception e) {
                Log.d("Error...", "Open Print Error...");
                //open error
                return "Not Available";
            }
        }

        private void closeDevice() {
            if (prnDevice != null) {
                try {
                    prnDevice.close();
                } catch (CustomException e) {
                    e.printStackTrace();
                }
            }
            prnDevice = null;
        }
    }
}
