package com.customamerica.custom_tradeshow_demo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;

public class TITOActivity extends AppCompatActivity implements OnClickListener {

    private final byte[] acceptTicket = {0x1f, 0x54, 0x48};
    private final byte[] getBarcode = {0x1f, 0x55};
    private final byte[] ejectTicket = {0x1f, 0x57, 0x45};
    private final byte[] retractTicket = {0x1f, 0x57, 0x52};
    private final byte[] setupPage = {
            0x1d, 0x7c, 0x04, //set print density
            0x1d, 0x50, 0x18, 0x18, //set horizontal and vertical motion units
            0x1b, 0x4c, //set page mode
            0x1b, 0x57, 0x08, 0x00, 0x00, 0x00, 0x38, 0x00, (byte) 0x8a, 0x00, //set printing area
            0x1b, 0x54, 0x33 //set print direction
    };
    private final byte[] barcodeCommand = {
            0x1d, 0x48, 0x02, //set HRI position
            0x1d, 0x77, (byte) 0x85, //set barcode width
            0x1d, 0x68, (byte) 0xaa, 0x00, //set barcode height
            0x1b, 0x24, 0x10, 0x00, 0x1d, 0x24, 0x30, 0x00,//set print position
            0x1d, 0x6b, 0x05 //start ITF barcode
    };
    private final byte[] barcode = {
            0x38, 0x30, 0x30, 0x31, 0x31,
            0x32, 0x38, 0x39, 0x37, 0x38,
            0x34, 0x32, 0x35, 0x33, 0x32,
            0x35, 0x32, 0x36, 0x37, 0x35,
            0x36, 0x33, 0x00
    };
    private final byte[] finishTicket = {
            0x1b, 0x21, 0x01,  //switch to font B and reset other font params
            0x1b, 0x24, 0x2f, 0x00, //set print position
            0x1d, 0x24, 0x1a, 0x00, //set print position
            0x44, 0x65, 0x6d, 0x6f, 0x20, //"Demo"
            0x54, 0x69, 0x63, 0x6b, 0x65, 0x74, 0x20, //"Ticket"
            0x43, 0x75, 0x73, 0x74, 0x6f, 0x6d, 0x20, //"Custom"
            0x41, 0x6d, 0x65, 0x72, 0x69, 0x63, 0x61, //"America"
            0x1b, 0x0c, //Print page mode
            0x1d, (byte) 0xf8, //Align ticket
            0x1d, 0x50, 0x00, 0x00, //reset horizontal and vertical motion units
            0x1b, 0x53, //exit page mode
            0x1c, 0x50, 0x06, 0x01, 0x45, (byte) 0xff //present
    };
    private CustomPrinter prnDevice;
    private TextView barcodeTextView;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tito);
        mContext = this;

        Toolbar mToolbar = (Toolbar) findViewById(R.id.activity_demo_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //set header to be the printer name
        TextView demoTitle = (TextView) findViewById(R.id.activity_text);
        prnDevice = PrinterFragment.prnDevice;
        assert prnDevice != null;
        demoTitle.setText(prnDevice.getPrinterName());

        //set up image view to hold scanned image
        barcodeTextView = (TextView) findViewById(R.id.barcode_text);

        //setup print button
        Button print = (Button) findViewById(R.id.print_button);
        print.setOnClickListener(this);

        //setup scan button
        Button scan = (Button) findViewById(R.id.scan_button);
        scan.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.scanner, menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            prnDevice.close();
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(@NonNull View v) {

        switch (v.getId()) {
            case R.id.scan_button:
                //set progress bar before starting a loop to wait for a scan
                setSupportProgressBarIndeterminateVisibility(true);
                //start async task to wait for scanned document
                new ScanTask(mContext).execute(null, null, false);
                //remove progress bar
                setSupportProgressBarIndeterminateVisibility(false);
                break;
            case R.id.print_button:
                print();
                break;
        }
    }

    private void print() {
        try {
            prnDevice.writeData(setupPage);
            prnDevice.writeData(barcodeCommand);
            prnDevice.writeData(barcode);
            prnDevice.writeData(finishTicket);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    private class ScanTask extends AsyncTask<Object, String, Boolean> {

        private final Context mContext;
        String barcodeValue;
        private ProgressDialog progressDialog;

        //constructor to give context for progress dialog
        public ScanTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            //create progress dialog that will not end until a document is scanned
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(getString(R.string.waitingToScan));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            barcodeTextView.setText("");

            try {
                //enable the feeder on the scanner and make the scanner hold the document
                prnDevice.writeData(acceptTicket);
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        protected Boolean doInBackground(Object... params) {
            try {
                int[] barcodeData = null;
                //wait for an image to be created by the scanner
                while (!prnDevice.getPrinterFullStatus().stsTICKETOUT) {
                    Thread.sleep(100);
                }

                while (barcodeData == null) {
                    prnDevice.writeData(getBarcode);
                    barcodeData = prnDevice.readData();
                }

                if (barcodeData[0] == 0x06) {
                    barcodeValue = "";
                    for (int i = 2; i < barcodeData.length; i++) {
                        barcodeValue += (barcodeData[i] - 48) + " ";
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (CustomException e) {
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(e.getMessage()) // text to display
                );
                e.printStackTrace();
                return false;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(getString(R.string.tito_eject_dialog_message))
                        .setTitle(R.string.tito_eject_dialog_title);

                builder.setNegativeButton(R.string.eject, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            prnDevice.writeData(ejectTicket);
                        } catch (CustomException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setPositiveButton(R.string.retract, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            prnDevice.writeData(retractTicket);
                        } catch (CustomException e) {
                            e.printStackTrace();
                        }
                    }
                });
                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
                barcodeTextView.setText(barcodeValue);
            } else {
                SnackbarManager.show(
                        Snackbar.with(mContext) // context
                                .colorResource(R.color.material_drawer_accent)
                                .text(getString(R.string.barcode_not_read)) // text to display
                );
            }

            //dismiss the progress dialog
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }
}
