package com.customamerica.custom_tradeshow_demo;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Created by bill on 12/17/14.
 */
class POSItemAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    // references to our images
    private final Food[] mFood = {
            new Food(R.drawable.ic_martini, "Martini", 9.50),
            new Food(R.drawable.ic_pizza, "Pizza", 2.25),
            new Food(R.drawable.ic_coffee, "Coffee", 1.89),
            new Food(R.drawable.ic_food, "Hamburger Meal", 11.75)
    };

    public POSItemAdapter(LayoutInflater inflater) {
        mInflater = inflater;
    }

    @Override
    public int getCount() {
        return mFood.length;
    }

    @Override
    public Object getItem(int position) {
        return mFood[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Nullable
    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {  // if it's not recycled, initialize some attributes

            convertView = mInflater.inflate(R.layout.pos_item, parent, false);

            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.pos_item_image);

            convertView.setTag(holder);
        } else {
            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        holder.thumbnailImageView.setImageResource(mFood[position].getId());
        return convertView;
    }

    private static class ViewHolder {
        public ImageView thumbnailImageView;
    }
}
