package com.customamerica.custom_tradeshow_demo;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import it.custom.printer.api.android.CustomException;
import it.custom.printer.api.android.CustomPrinter;


public class POSActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private final byte[] enableCompression = {0x1b, 0x2a, 0x62, 0x32, 0x4d};
    private final byte[] setPageSize = {0x1b, 0x26, 0x6c, 0x35, 0x30, 0x52};
    private final byte[] blackLine = {0x1b, 0x2a, 0x62, 0x33, 0x57, (byte) 0xff, (byte) 0xff, (byte) 0xc0};
    private final byte[] printGAM = {0x1b, 0x2a, 0x72, 0x42};
    private final byte[] printSetup = {0x1d, (byte) 0xff, 0x68, 0x30, (byte) 0xb4};
    private final byte[] openCover = {0x1b, (byte) 0xb4};
    private byte[] printLine;
    private CustomPrinter prnDevice;
    private String printerName;
    private ArrayList<Food> itemOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos);
        createLineByteArray();

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.activity_demo_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prnDevice = PrinterFragment.prnDevice;
        printerName = prnDevice.getPrinterName();

        GridView gridView = (GridView) findViewById(R.id.item_grid);
        gridView.setAdapter(new POSItemAdapter(getLayoutInflater()));
        gridView.setOnItemClickListener(this);

        Button printButton = (Button) findViewById(R.id.print_button);
        printButton.setOnClickListener(this);

        ((TextView) findViewById(R.id.pos_activity_text)).setText(printerName);

        itemOrder = new ArrayList<>();
        itemOrder.clear();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pos_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_print_setup) {
            printSetup();
        } else if (id == R.id.action_cover_open) {
            openCover();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createLineByteArray() {
        //concatenate the GAM commands to make a line
        //this lessens the amount of times to open the pipe to the printer
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            outputStream.write(setPageSize);
            outputStream.write(enableCompression);
            outputStream.write(blackLine);
            outputStream.write(blackLine);
            outputStream.write(printGAM);
        } catch (IOException e) {
            e.printStackTrace();
        }

        printLine = outputStream.toByteArray();
    }

    private void print() {
        try {
            DecimalFormat decimalFormat = new DecimalFormat("#.00");

            if (printerName.contains("Q1")) {
                printerName = "Custom Q3";
            }

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.custom_logo);
            prnDevice.printImage(bm, CustomPrinter.IMAGE_ALIGN_TO_CENTER, CustomPrinter.IMAGE_SCALE_TO_WIDTH, 300);
            prnDevice.feed(1);

            prnDevice.printTextLF(getString(R.string.address1), PrinterFragment.fontHeader);
            prnDevice.printTextLF(getString(R.string.address2), PrinterFragment.fontHeader);
            prnDevice.printTextLF(getString(R.string.phone), PrinterFragment.fontHeader);
            prnDevice.feed(1);

            prnDevice.printText(getString(R.string.ticket_printer_name_text) + " ", PrinterFragment.fontNormal);
            prnDevice.printTextLF(printerName, PrinterFragment.fontNormal);

            prnDevice.writeData(printLine);
            prnDevice.feed(1);

            double totalPrice = 0;
            for (int i = 0; i < itemOrder.size(); i++) {
                prnDevice.printTextLF(itemOrder.get(i).getName(), PrinterFragment.fontNormal);
                prnDevice.printTextLF("$" + decimalFormat.format(itemOrder.get(i).getPrice()), PrinterFragment.fontRight);
                totalPrice += itemOrder.get(i).getPrice();
            }

            prnDevice.writeData(printLine);
            prnDevice.feed(1);

            prnDevice.printTextLF("Total:", PrinterFragment.fontNormal);
            prnDevice.printTextLF("$" + decimalFormat.format(totalPrice), PrinterFragment.fontRight);
            prnDevice.feed(1);

            prnDevice.printTextLF(getString(R.string.ticket_scan_text), PrinterFragment.fontHeader);
            prnDevice.feed(1);

            String url = getString(R.string.base_brochure_url);
            if (printerName.toUpperCase().contains("K3")) {
                url += getString(R.string.k3_url);
            } else if (printerName.toUpperCase().contains("Q3")) {
                url += getString(R.string.q3_url);
            } else if (printerName.toUpperCase().contains("KUBE")) {
                url += getString(R.string.kube_url);
            } else if (printerName.toUpperCase().contains("D-ONE")) {
                url += getString(R.string.done_url);
            } else {
                url = getString(R.string.custom_url);
            }
            prnDevice.printBarcode2D(url, CustomPrinter.BARCODE_TYPE_QRCODE, CustomPrinter.BARCODE_ALIGN_TO_CENTER, 150);
            prnDevice.feed(4);

            if (printerName.toUpperCase().contains("D-ONE")) {
                prnDevice.feed(2);
            }
            prnDevice.cut(CustomPrinter.CUT_PARTIAL);

        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    private void printSetup() {
        try {
            prnDevice.writeData(printSetup);
        } catch (CustomException e) {
            e.printStackTrace();
        }
    }

    private void openCover() {
        if (printerName.contains("FUSION") || printerName.contains("K3")) {
            try {
                prnDevice.writeData(openCover);
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (itemOrder.size() <= 0) {
            SnackbarManager.show(
                    Snackbar.with(this)
                            .colorResource(R.color.material_drawer_accent)
                            .text(getString(R.string.pos_enter_an_item)));
        } else {
            print();
            itemOrder.clear();
        }
    }

    @Override
    public void onItemClick(@NonNull AdapterView<?> parent, View view, int position, long id) {
        Food tempFood = (Food) parent.getItemAtPosition(position);

        // Dismisses the SnackBar being shown, if any, and displays the new one
        SnackbarManager.show(
                Snackbar.with(this)
                        .colorResource(R.color.material_drawer_accent)
                        .text(tempFood.getName() + " added to the order!")
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT));

        itemOrder.add(tempFood);
    }
}
