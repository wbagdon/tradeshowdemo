# README #

### What is this repository for? ###

* Base Android application for tradeshows to demo WiFi, Bluetooth and USB
* Version 1.1

### How do I get set up? ###

* 1) Download Android Studio: 
> http://developer.android.com/sdk/index.html
* 2) Install BitBucket plugin
> https://bitbucket.org/dmitry_cherkas/jetbrains-bitbucket-connector/downloads/jetbrains-bitbucket-connector_IC-139.224.zip
* 3) Clone repo from BitBucket